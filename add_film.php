<?php 
include_once('db.php');

connect('localhost', 'root', '', 'actorsdb', function() {
	if ($_POST) {
		request("SELECT MAX(films.film_id) AS `last index` FROM actorsdb.films", true, function($response) {
			$id        = $response[0]['last index'] + 1;
			$title     = $_POST['title'];
			$country   = $_POST['country'];
			$announced = $_POST['announced'];
			$actor     = $_GET['actor_id'];
			$sql = "INSERT INTO actorsdb.films (film_id, actor, title, country, announced) VALUES('$id', '$actor','$title', '$country', '$announced')";
			request($sql, false, function() { header('Location: index.php'); });
		});
	}
});	// Fconnect
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Add film</title>
	<link rel="stylesheet" type="text/css" href="static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme-flatly.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Add new film</h3>
			</div>
			<div class="panel-body">
				<form action="<?php $_PHP_SELF ?>" method="POST" role="form">
					<div class="form-group">
						<label for="name">Title</label>
						<input type="text" class="form-control" id="title" name="title" placeholder="Film title" />
					</div>
					<div class="form-group">
						<label for="country">Country</label>
						<input type="text" class="form-control" id="country" name="country" size=6  placeholder="Chicago" />
					</div>
					<div class="form-group">
						<label for="announced">Annouced</label>
						<input type="date" class="form-control" id="announced" name="announced" />
					</div>
					<div class="flex">
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-warning">Reset</button>
						<a href="index.php?id=<?php echo $_GET['actor_id']; ?>" class="btn btn-danger">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>