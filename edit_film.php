<?php 
	include_once('db.php');

	$film = [];
	$id = $_GET['id'];

	// TODO: refactoring - delete squared brackets
	connect('localhost', 'root', '', 'actorsdb', function() {
		$id = $GLOBALS['id'];

		if ($_POST) {
			$title = $_POST['title'];
			$country = $_POST['country'];
			$announced = $_POST['announced'];
			$sql = "UPDATE actorsdb.films SET title='$title', country='$country', announced='$announced' WHERE film_id = $id";

			request($sql, false, function() { 
				header('Location: index.php?id='.$GLOBALS['id']);
			});
		}

		request("SELECT * FROM actorsdb.films WHERE film_id = $id", true, function($response) {
			$GLOBALS['film'] = $response[0];
		});
	});
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edit film</title>
	<link rel="stylesheet" type="text/css" href="static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme-flatly.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<form action="<?php $_PHP_SELF."?id=".$_GET['id'] ?>" method="POST" role="form">
			<legend>Edit film info</legend>
			<div class="form-group">
				<label for="title">Name</label>
				<input type="text" class="form-control" id="title" name="title" value=<?php echo $film['title'] ?>>
			</div>
			<div class="form-group">
				<label for="country">Gender</label>
				<input type="text" class="form-control" id="country" name="country" size=15  value=<?php echo $film['country'] ?>>
			</div>
			<div class="form-group">
				<label for="announced">Announsed</label>
				<input type="text" class="form-control" id="announced" name="announced" size=15 value=<?php echo $film['announced'] ?>>
			</div>
			<div class="flex">
				<button type="submit" class="btn btn-primary">Save</button>
				<button type="reset" class="btn btn-warning">Reset</button>
				<form action="delete_student.php?id=<?php echo $id; ?>" method="POST" onsubmit="return confirm('Are You sure?');">
					<button type="submit" class="btn btn-primary">Delete Film</button>
				</form>
				<a href="index.php?id=2" class="btn btn-danger">Cancel</a>
			</div>
		</form>
	</div>
</body>
</html>