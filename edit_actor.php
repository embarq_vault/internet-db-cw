<?php 
	include_once('db.php');

	$actor = [];
	$id = $_GET['id'];

	// TODO: refactor - delete squared-brackets
	connect('localhost', 'root', '', 'actorsdb', function() {
		$id = $GLOBALS['id'];

		if ($_POST) {
			$name = $_POST['name'];
			$country = $_POST['country'];
			$birthdate = $_POST['birthdate'];
			$sql = "UPDATE actorsdb.actors SET name='$name', country='$country', birthdate='$birthdate' WHERE id = $id";

			request($sql, false, function() { 
				header('Location: index.php');
			});
		}

		request("SELECT * FROM actorsdb.actors WHERE id = $id", true, function($response) {
			$GLOBALS['actor'] = $response[0];
		});
	}); // Fconnect
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Edit actor</title>
	<link rel="stylesheet" type="text/css" href="static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme-flatly.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<form action="<?php $_PHP_SELF."?id=".$_GET['id'] ?>" method="POST" role="form">
			<legend>Edit actor info</legend>
			<div class="form-group">
				<label for="name">Name</label>
				<input type="text" class="form-control" id="name" name="name" value=<?php echo $actor['name'] ?>>
			</div>
			<div class="form-group">
				<label for="country">Gender</label>
				<input type="text" class="form-control" id="country" name="country" size=15  value=<?php echo $actor['country'] ?>>
			</div>
			<div class="form-group">
				<label for="birthdate">Birthdate</label>
				<input type="text" class="form-control" id="birthdate" name="birthdate" size=15 value=<?php echo $actor['birthdate'] ?>>
			</div>
			<div class="flex">
				<button type="submit" class="btn btn-primary">Save</button>
				<button type="reset" class="btn btn-warning">Reset</button>
				<form action="delete_student.php?id=<?php echo $id; ?>" method="POST" onsubmit="return confirm('Are You sure?');">
					<button type="submit" class="btn btn-danger">Delete Actor</button>
				</form>
				<a href="index.php?id=2" class="btn btn-danger">Cancel</a>
			</div>
		</form>
	</div>
</body>
</html>