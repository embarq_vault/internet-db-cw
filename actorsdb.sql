-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 25 2016 г., 09:33
-- Версия сервера: 5.5.48
-- Версия PHP: 5.4.45

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `actorsdb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `actors`
--

CREATE TABLE IF NOT EXISTS `actors` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `country` varchar(15) NOT NULL,
  `birthdate` date NOT NULL,
  `photo` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `actors`
--

INSERT INTO `actors` (`id`, `name`, `country`, `birthdate`, `photo`) VALUES
(1, 'Marina Anna', 'Kenya', '1925-03-01', 'https://robohash.org/quamvelaut.bmp?size=220x220&set=set1'),
(2, 'Phyllis Banks', 'China', '1925-05-18', 'https://robohash.org/nemovelitut.bmp?size=220x220&set=set1'),
(3, 'Joyce Bennett', 'Portugal', '1941-06-16', 'https://robohash.org/quiaetet.bmp?size=220x220&set=set1'),
(4, 'Dorothy Cox', 'Brazil', '1987-07-19', 'https://robohash.org/aliasautex.bmp?size=220x220&set=set1'),
(5, 'Terry Ward', 'Paraguay', '1944-04-13', 'https://robohash.org/dolorametut.bmp?size=220x220&set=set1'),
(6, 'Mark Watson', 'Honduras', '1943-08-28', 'https://robohash.org/nullaculpavoluptatem.bmp?size=220x220&set=set1'),
(7, 'Walter Lawrence', 'Poland', '1960-07-09', 'https://robohash.org/inciduntperferendisnon.bmp?size=220x220&set=set1'),
(8, 'Richard Porter', 'Philippines', '1952-04-11', 'https://robohash.org/nonsitqui.bmp?size=220x220&set=set1'),
(9, 'Ryan Thompson', 'New Caledonia', '1924-12-29', 'https://robohash.org/quinequenostrum.bmp?size=220x220&set=set1'),
(10, 'Marie Allen', 'Sweden', '1969-06-09', 'https://robohash.org/deseruntrecusandaequaerat.bmp?size=220x220&set=set1'),
(11, 'Kathryn Rodriguez', 'Peru', '1929-09-21', 'https://robohash.org/autemquoseum.bmp?size=220x220&set=set1'),
(12, 'Linda Elliott', 'Serbia', '1957-06-01', 'https://robohash.org/delectuseaqueodio.bmp?size=220x220&set=set1'),
(13, 'Kathleen Perry', 'China', '1938-01-10', 'https://robohash.org/nihilearerum.bmp?size=220x220&set=set1'),
(14, 'Emily Gray', 'Poland', '1985-09-29', 'https://robohash.org/etquiadignissimos.bmp?size=220x220&set=set1'),
(15, 'Michelle Edwards', 'Chile', '1903-09-03', 'https://robohash.org/eiusutfugiat.bmp?size=220x220&set=set1'),
(16, 'Louise Murray', 'Peru', '1941-11-04', 'https://robohash.org/quiaiurequisquam.bmp?size=220x220&set=set1'),
(17, 'Ronald Nguyen', 'Malta', '1925-05-22', 'https://robohash.org/saepeestcommodi.bmp?size=220x220&set=set1'),
(18, 'Marilyn Green', 'Australia', '1953-05-29', 'https://robohash.org/nisiofficiisab.bmp?size=220x220&set=set1'),
(19, 'Michael Flores', 'Sweden', '1941-12-20', 'https://robohash.org/etquodquos.bmp?size=220x220&set=set1'),
(20, 'Christina Nguyen', 'Philippines', '1910-07-29', 'https://robohash.org/asperioresaliquamsoluta.bmp?size=220x220&set=set1'),
(21, 'Nicole Jenkins', 'Japan', '1924-08-16', 'https://robohash.org/estvoluptatesvoluptatum.bmp?size=220x220&set=set1'),
(22, 'Marilyn Griffin', 'Indonesia', '1921-03-23', 'https://robohash.org/sintinventoreratione.bmp?size=220x220&set=set1'),
(23, 'Carolyn Hall', 'Argentina', '1942-10-19', 'https://robohash.org/remdoloresqui.bmp?size=220x220&set=set1'),
(24, 'Andrew Hanson', 'Mauritania', '1972-11-21', 'https://robohash.org/inciduntexpeditavoluptatem.bmp?size=220x220&set=set1'),
(25, 'Linda Ray', 'United States', '1944-01-13', 'https://robohash.org/culpamolestiaecorrupti.bmp?size=220x220&set=set1'),
(26, 'Doris Sims', 'Ukraine', '1934-07-09', 'https://robohash.org/rerumnonet.bmp?size=220x220&set=set1'),
(27, 'Wayne Spencer', 'Nigeria', '1929-01-14', 'https://robohash.org/omnisenimad.bmp?size=220x220&set=set1'),
(28, 'Kathleen Brown', 'Palestinian Ter', '1983-06-13', 'https://robohash.org/nostrummollitiaaspernatur.bmp?size=220x220&set=set1'),
(29, 'Jacqueline Frazier', 'Indonesia', '1939-09-22', 'https://robohash.org/suntperspiciatismagnam.bmp?size=220x220&set=set1'),
(30, 'Charles Shaw', 'China', '1913-01-02', 'https://robohash.org/exincidunteligendi.bmp?size=220x220&set=set1'),
(31, 'Kate Watson', 'Chicago', '2016-05-16', '');

-- --------------------------------------------------------

--
-- Структура таблицы `films`
--

CREATE TABLE IF NOT EXISTS `films` (
  `film_id` int(11) NOT NULL,
  `actor` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `country` varchar(15) NOT NULL,
  `announced` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `films`
--

INSERT INTO `films` (`film_id`, `actor`, `title`, `country`, `announced`) VALUES
(1, 9, 'Malayalam Yearly ultrices', 'Lebanon', '1994-05-13'),
(2, 2, 'Hebrew Once risus', 'China', '1931-08-16'),
(3, 19, 'Montenegrin Never sed', 'France', '1986-12-10'),
(4, 9, 'Filipino Seldom pretium q', 'Peru', '1941-01-20'),
(5, 19, 'Estonian Monthly molestie', 'Armenia', '1943-02-28'),
(6, 2, 'Somali Once integer ac', 'Philippines', '2010-08-25'),
(7, 30, 'Hebrew Never rhoncus aliq', 'Bangladesh', '2010-02-04'),
(8, 14, 'Lao Never massa quis', 'Hungary', '1933-05-17'),
(9, 8, 'Japanese Never volutpat d', 'China', '1945-08-14'),
(10, 1, 'Norwegian Sell', 'Germany', '1977-06-11'),
(11, 24, 'Tamil Seldom morbi odio', 'Sweden', '1937-08-07'),
(12, 25, 'Danish Weekly nec', 'China', '1979-09-17'),
(13, 28, 'Somali Often condimentum', 'China', '1972-01-03'),
(14, 15, 'Thai Seldom congue', 'Cuba', '1963-07-23'),
(15, 18, 'Norwegian Never ac', 'China', '1982-10-21'),
(16, 23, 'Japanese Never sapien', 'China', '2007-03-09'),
(17, 3, 'Tswana Daily aliquam', 'France', '1947-08-14'),
(18, 13, 'German Never tempor', 'Philippines', '1969-05-22'),
(19, 21, 'Quechua Seldom iaculis ju', 'Indonesia', '1993-07-17'),
(20, 11, 'Hiri Motu Monthly sit', 'Uzbekistan', '2014-12-27'),
(21, 22, 'Dhivehi Monthly amet eros', 'China', '1948-02-12'),
(22, 22, 'Italian Seldom ullamcorpe', 'Czech Republic', '1977-08-10'),
(23, 14, 'Albanian Yearly nibh', 'Philippines', '1946-10-30'),
(24, 7, 'Korean Yearly morbi', 'Argentina', '1930-12-22'),
(25, 9, 'Thai Yearly ut odio', 'China', '2006-06-20'),
(26, 28, 'Northern Sotho Seldom viv', 'Comoros', '1948-12-13'),
(27, 1, 'Gujarati Weekly vel lectu', 'Brazil', '1975-04-14'),
(28, 24, 'Kashmiri Never nulla', 'Norway', '2004-03-05'),
(29, 17, 'Tswana Often ligula', 'Indonesia', '1932-09-19'),
(30, 16, 'Dutch Weekly in felis', 'Sweden', '2014-02-15'),
(31, 22, 'Korean Never pellentesque', 'Kazakhstan', '1963-03-31'),
(32, 16, 'Italian Daily turpis', 'China', '1944-12-11'),
(33, 18, 'Irish Gaelic Weekly sit', 'Czech Republic', '2005-03-02'),
(34, 26, 'Japanese Often sem', 'Indonesia', '1992-03-23'),
(35, 13, 'Yiddish Yearly ultrices', 'France', '1976-05-10'),
(36, 28, 'Persian Never interdum', 'Cuba', '1956-06-25'),
(37, 8, 'Gujarati Daily morbi', 'Armenia', '1978-02-17'),
(38, 5, 'Gujarati Once quis turpis', 'Russia', '1954-06-11'),
(39, 10, 'Georgian Once ornare cons', 'China', '1944-05-14'),
(40, 12, 'Polish Monthly tortor', 'Philippines', '1973-09-16'),
(41, 23, 'Georgian Weekly sollicitu', 'Croatia', '1973-07-15'),
(42, 2, 'Belarusian Never lacinia', 'New Zealand', '1998-11-10'),
(43, 6, 'Japanese Monthly rhoncus', 'Tunisia', '2012-07-13'),
(44, 7, 'Haitian Creole Once natoq', 'China', '1956-11-07'),
(45, 4, 'Norwegian Once curabitur ', 'China', '1977-05-14'),
(46, 14, 'Mongolian Yearly nibh', 'Peru', '2014-03-09'),
(47, 15, 'Danish Seldom amet', 'Russia', '1997-09-03'),
(48, 7, 'New Zealand Sign Language', 'Greece', '1997-01-19'),
(49, 2, 'Bengali Seldom velit done', 'Germany', '1965-03-30'),
(50, 9, 'Tajik Weekly lorem', 'Iceland', '1988-07-27'),
(51, 26, 'M?ori Seldom libero quis', 'Albania', '1944-06-13'),
(52, 14, 'Finnish Seldom enim lorem', 'Macedonia', '1985-11-05'),
(53, 12, 'Estonian Once pellentesqu', 'Cameroon', '2015-06-14'),
(54, 3, 'Greek Weekly mi nulla', 'China', '2012-10-23'),
(55, 1, 'Polish Yearly aliquet mae', 'China', '1992-05-22'),
(56, 17, 'Yiddish Once id massa', 'China', '1941-10-27'),
(57, 28, 'Filipino Weekly sapien', 'China', '1979-07-07'),
(58, 10, 'Armenian Never nec nisi', 'Portugal', '2012-05-02'),
(59, 21, 'Tajik Never dignissim', 'China', '1998-07-04'),
(60, 26, 'M?ori Daily vitae ipsum', 'China', '1957-09-15'),
(61, 9, 'Croatian Often felis eu', 'Kazakhstan', '1954-11-06'),
(62, 20, 'Dari Seldom tortor id', 'China', '1943-03-23'),
(63, 3, 'Sotho Daily duis consequa', 'Afghanistan', '1954-04-14'),
(64, 1, 'Haitian Creole Never dapi', 'China', '1947-09-11'),
(65, 19, 'Macedonian Yearly justo', 'Philippines', '1954-07-04'),
(66, 24, 'Kazakh Seldom dis', 'China', '1966-02-17'),
(67, 12, 'Montenegrin Yearly quis j', 'China', '1958-01-29'),
(68, 27, 'Khmer Weekly accumsan tor', 'France', '2007-01-03'),
(69, 27, 'Assamese Often at ipsum', 'Colombia', '2001-09-05'),
(70, 2, 'Indonesian Often libero', 'South Korea', '1956-01-13'),
(71, 6, 'Finnish Weekly mauris', 'Costa Rica', '1973-07-18'),
(72, 13, 'Zulu Monthly sit amet', 'China', '2009-08-14'),
(73, 20, 'Maltese Weekly cum sociis', 'Philippines', '1941-10-04'),
(74, 2, 'Khmer Monthly nulla', 'China', '1974-09-11'),
(75, 14, 'Aymara Never quis turpis', 'China', '1996-09-10'),
(76, 17, 'Montenegrin Daily erat', 'Sweden', '1997-01-31'),
(77, 13, 'Swati Once natoque', 'Russia', '1975-10-09'),
(78, 12, 'Hindi Never cras non', 'China', '2001-08-21'),
(79, 20, 'Fijian Weekly nulla', 'Brazil', '1932-11-05'),
(80, 30, 'Malayalam Yearly et eros', 'Nigeria', '1998-06-10'),
(81, 28, 'Sotho Monthly venenatis l', 'United Kingdom', '2013-04-14'),
(82, 27, 'Kazakh Yearly in purus', 'Afghanistan', '1990-07-28'),
(83, 13, 'Quechua Daily lacinia', 'United States', '1976-11-05'),
(84, 2, 'Quechua Yearly orci', 'South Sudan', '2005-02-19'),
(85, 7, 'Swahili Never nisi', 'Russia', '1987-06-09'),
(86, 11, 'Oriya Monthly nulla', 'Russia', '2015-09-10'),
(87, 21, 'Telugu Often cursus id', 'France', '1951-03-15'),
(88, 17, 'Malay Often id pretium', 'China', '1974-04-26'),
(89, 26, 'Hungarian Seldom amet', 'Indonesia', '1972-06-29'),
(90, 2, 'Kurdish Once ultrices', 'Canada', '1979-07-24'),
(91, 25, 'Arabic Weekly dictumst mo', 'China', '1955-08-28'),
(92, 14, 'Hebrew Never justo sollic', 'Philippines', '1939-08-24'),
(93, 10, 'Hebrew Never ipsum', 'Japan', '2011-03-21'),
(94, 15, 'Maltese Once nulla nisl', 'Macedonia', '1932-08-03'),
(95, 8, 'Tok Pisin Monthly rutrum ', 'Peru', '1964-10-04'),
(96, 29, 'Dutch Never erat nulla', 'Syria', '2015-03-14'),
(97, 4, 'Albanian Weekly non ligul', 'Serbia', '1998-01-25'),
(98, 24, 'Fijian Never nulla', 'Japan', '1959-01-04'),
(99, 19, 'Belarusian Weekly nunc', 'South Africa', '1931-03-15'),
(100, 8, 'Thai Seldom felis sed', 'Syria', '1972-02-26'),
(101, 25, 'Mongolian Monthly sapien', 'Czech Republic', '1960-03-14'),
(102, 7, 'Thai Often ac', 'Thailand', '2014-05-16'),
(103, 13, 'Quechua Weekly aliquam', 'Nigeria', '2009-08-16'),
(104, 3, 'Norwegian Never convallis', 'Benin', '1987-02-21'),
(105, 6, 'Norwegian Seldom posuere ', 'Peru', '1936-07-12'),
(106, 29, 'Fijian Often at ipsum', 'Ukraine', '1997-12-19'),
(107, 15, 'Belarusian Never nulla qu', 'Peru', '1986-08-08'),
(108, 25, 'Belarusian Yearly integer', 'Russia', '1967-11-08'),
(109, 26, 'Filipino Once eget tincid', 'Finland', '2010-10-01'),
(110, 8, 'Macedonian Seldom augue', 'China', '1971-10-07'),
(111, 26, 'Sotho Weekly nec sem', 'China', '1944-10-22'),
(112, 18, 'Hindi Once integer', 'Iran', '1976-01-05'),
(113, 28, 'Gagauz Daily vitae consec', 'Brazil', '1951-06-20'),
(114, 20, 'Haitian Creole Once erat', 'Albania', '2015-09-23'),
(115, 27, 'Moldovan Seldom etiam fau', 'Albania', '1954-12-17'),
(116, 6, 'Swati Seldom nec', 'Philippines', '2015-09-23'),
(117, 28, 'Indonesian Yearly maecena', 'Brazil', '1956-02-25'),
(118, 6, 'Sotho Yearly sed lacus', 'Indonesia', '2002-02-18'),
(119, 25, 'Swahili Yearly nibh quisq', 'China', '1970-06-01'),
(120, 27, 'Kashmiri Monthly dolor', 'France', '1930-03-29'),
(121, 26, 'Finnish Once amet sem', 'Philippines', '1998-12-15'),
(122, 3, 'Georgian Often nulla', 'Indonesia', '1982-02-12'),
(123, 1, 'Assamese Seldom vestibulu', 'Russia', '1988-03-10'),
(124, 6, 'Bislama Daily sit amet', 'China', '1948-04-28'),
(125, 15, 'Thai Monthly placerat', 'Sweden', '2004-01-17'),
(126, 3, 'Telugu Daily vestibulum', 'China', '1995-11-09'),
(127, 25, 'Mongolian Often ridiculus', 'China', '1969-01-02'),
(128, 23, 'Macedonian Daily venenati', 'Japan', '1951-11-28'),
(129, 23, 'Lao Seldom commodo', 'Canada', '1971-09-15'),
(130, 13, 'Kazakh Weekly magna', 'Pakistan', '1936-03-06'),
(131, 17, 'Oriya Daily neque aenean', 'Portugal', '1968-01-11'),
(132, 22, 'Ndebele Weekly feugiat', 'Thailand', '1967-02-28'),
(133, 3, 'Dari Weekly sit', 'Argentina', '2015-01-27'),
(134, 16, 'Hebrew Daily augue vestib', 'Ethiopia', '1997-08-13'),
(135, 27, 'Bengali Once nulla suscip', 'Philippines', '2002-12-30'),
(136, 1, 'Swedish Never nonummy mae', 'Russia', '1944-05-22'),
(137, 25, 'Icelandic Once curabitur', 'Japan', '1934-05-26'),
(138, 3, 'Assamese Once vulputate', 'Argentina', '1982-02-25'),
(139, 19, 'Dari Seldom tortor', 'Portugal', '2002-07-27'),
(140, 18, 'Polish Once gravida', 'Japan', '1930-06-19'),
(141, 3, 'Luxembourgish Once ipsum ', 'Poland', '1988-11-11'),
(142, 22, 'Estonian Monthly placerat', 'Greece', '1987-07-05'),
(143, 7, 'Icelandic Seldom ligula i', 'Russia', '2010-06-18'),
(144, 29, 'Somali Seldom nulla', 'Czech Republic', '2010-03-30'),
(145, 16, 'Kashmiri Seldom mi in', 'Poland', '1990-04-10'),
(146, 10, 'Tswana Seldom nunc', 'China', '1935-09-03'),
(147, 14, 'Estonian Often rutrum', 'China', '1992-07-17'),
(148, 11, 'Zulu Monthly faucibus', 'Sweden', '1958-10-04'),
(149, 5, 'Malay Seldom amet erat', 'France', '2014-03-27'),
(150, 1, 'Persian Seldom consequat ', 'Finland', '2007-04-08'),
(151, 5, 'Irish Gaelic Never donec', 'Bulgaria', '1932-08-09'),
(152, 30, 'Kannada Often neque duis', 'Nigeria', '1963-06-30'),
(153, 20, 'Japanese Often ac', 'Armenia', '1958-09-22'),
(154, 15, 'Kurdish Often at ipsum', 'Palau', '1937-08-02'),
(155, 22, 'Oriya Daily in eleifend', 'Czech Republic', '1989-08-10'),
(156, 1, 'Bislama Weekly vel', 'Argentina', '2013-02-24'),
(157, 27, 'Danish Often pede', 'Guatemala', '2008-02-07'),
(158, 30, 'Ndebele Weekly mauris', 'Nicaragua', '1962-01-02'),
(159, 20, 'M?ori Never ipsum ac', 'Madagascar', '1966-01-31'),
(160, 27, 'Gagauz Monthly sapien', 'Cape Verde', '2002-05-18'),
(161, 5, 'Lao Never magnis', 'Brazil', '1990-07-03'),
(162, 19, 'Filipino Never duis', 'China', '1993-04-10'),
(163, 19, 'Hiri Motu Daily morbi vel', 'China', '2002-09-11'),
(164, 20, 'Tamil Often urna', 'Brazil', '1965-06-21'),
(165, 1, 'Lao Once ac tellus', 'Peru', '1989-12-12'),
(166, 30, 'Amharic Monthly posuere', 'China', '1934-10-01'),
(167, 29, 'Swati Daily cubilia curae', 'Tajikistan', '1934-11-22'),
(168, 18, 'Hebrew Once vestibulum an', 'Armenia', '2013-09-15'),
(169, 27, 'Georgian Yearly sapien', 'Canada', '1967-12-14'),
(170, 12, 'Papiamento Never convalli', 'Peru', '1977-01-29'),
(171, 25, 'Malay Never tincidunt in', 'Argentina', '1974-07-08'),
(172, 26, 'Filipino Once lacus', 'China', '1976-07-26'),
(173, 11, 'Gagauz Yearly fermentum j', 'Japan', '1931-10-20'),
(174, 18, 'Thai Often at', 'Canada', '1998-04-19'),
(175, 21, 'Papiamento Monthly purus ', 'China', '1969-11-09'),
(176, 27, 'Punjabi Once sit', 'Indonesia', '1968-09-01'),
(177, 1, 'New Zealand Sign Language', 'Indonesia', '2014-04-19'),
(178, 17, 'Hebrew Yearly volutpat', 'Philippines', '2014-06-07'),
(179, 13, 'Mongolian Monthly ligula', 'Portugal', '1952-02-08'),
(180, 1, 'Tajik Weekly nunc', 'Afghanistan', '2004-07-21'),
(181, 23, 'Hindi Daily vestibulum', 'Philippines', '2000-12-02'),
(182, 9, 'Hiri Motu Seldom ut dolor', 'Indonesia', '2004-09-27'),
(183, 20, 'M?ori Never ipsum', 'Mauritius', '1960-04-01'),
(184, 30, 'Kazakh Yearly ipsum primi', 'China', '1956-06-18'),
(185, 22, 'Czech Once proin', 'Czech Republic', '2015-05-21'),
(186, 22, 'Finnish Once nunc viverra', 'China', '1985-10-11'),
(187, 23, 'Hebrew Once magna at', 'France', '1936-03-16'),
(188, 30, 'Italian Often sodales sed', 'France', '1980-12-05'),
(189, 4, 'Albanian Monthly erat', 'France', '1969-06-30'),
(190, 8, 'Khmer Never odio donec', 'Portugal', '2011-10-31'),
(191, 28, 'Romanian Yearly posuere', 'Sweden', '1963-10-01'),
(192, 6, 'Filipino Often in lacus', 'Russia', '1980-02-06'),
(193, 12, 'Portuguese Monthly sapien', 'Indonesia', '1987-06-04'),
(194, 26, 'Indonesian Monthly in', 'China', '1956-11-15'),
(195, 21, 'Japanese Seldom orci', 'China', '1980-07-07'),
(196, 2, 'Swati Once etiam pretium', 'Colombia', '1982-10-22'),
(197, 28, 'Kurdish Monthly ut', 'China', '1959-12-13'),
(198, 18, 'Italian Seldom et', 'Syria', '1936-08-08'),
(199, 2, 'Hungarian Monthly interdu', 'Colombia', '1938-11-01'),
(200, 13, 'Latvian Daily vestibulum', 'Indonesia', '1946-07-08'),
(201, 28, 'Malagasy Seldom pulvinar ', 'Indonesia', '1979-11-20'),
(202, 28, 'Papiamento Daily feugiat', 'Georgia', '2003-07-10'),
(203, 15, 'Yiddish Once vestibulum', 'Japan', '2010-08-22'),
(204, 11, 'Japanese Monthly dolor qu', 'Russia', '1995-08-20'),
(205, 26, 'Kyrgyz Yearly vel nisl', 'Greece', '1953-02-16'),
(206, 7, 'Hungarian Yearly amet', 'Russia', '2004-09-19'),
(207, 22, 'Nepali Once libero', 'Japan', '2004-06-19'),
(208, 25, 'Chinese Yearly nibh', 'Brazil', '1960-04-02'),
(209, 30, 'Portuguese Weekly erat', 'China', '1936-02-01'),
(210, 15, 'Danish Seldom nam', 'United States', '1986-03-29'),
(211, 2, 'Indonesian Yearly dui nec', 'Norway', '2011-06-25'),
(212, 7, 'Hiri Motu Seldom nulla', 'Russia', '1963-01-01'),
(213, 7, 'Maltese Weekly sit amet', 'Poland', '1993-08-25'),
(214, 16, 'Lao Yearly rutrum nulla', 'Brazil', '1973-03-26'),
(215, 18, 'Bengali Daily id pretium', 'Indonesia', '1997-03-02'),
(216, 30, 'Korean Daily justo morbi', 'Greenland', '1954-05-02'),
(217, 24, 'Lithuanian Yearly turpis', 'Guatemala', '2003-02-06'),
(218, 7, 'Zulu Never augue', 'South Africa', '1942-02-03'),
(219, 16, 'Chinese Monthly felis', 'China', '1980-04-30'),
(220, 5, 'Bosnian Often tempor', 'China', '1970-05-25'),
(221, 14, 'Oriya Daily sollicitudin', 'China', '1958-10-10'),
(222, 22, 'Khmer Seldom suspendisse', 'Finland', '1934-07-23'),
(223, 18, 'Croatian Yearly vel', 'Thailand', '2007-04-03'),
(224, 11, 'Spanish Daily lacus', 'Canada', '2014-07-08'),
(225, 8, 'Greek Monthly sed', 'Iran', '1939-07-20'),
(226, 4, 'Tswana Monthly at dolor', 'Guatemala', '1930-10-10'),
(227, 23, 'English Once posuere', 'Indonesia', '1981-05-04'),
(228, 26, 'Mongolian Weekly sed nisl', 'Sweden', '1950-11-10'),
(229, 30, 'Tsonga Daily molestie lor', 'Tanzania', '1948-04-22'),
(230, 18, 'Sotho Seldom risus praese', 'Italy', '1932-07-01'),
(231, 29, 'Tswana Often luctus et', 'Vietnam', '2000-04-05'),
(232, 14, 'Marathi Weekly turpis a', 'Mayotte', '1955-10-09'),
(233, 21, 'English Monthly rhoncus a', 'Vietnam', '1997-08-29'),
(234, 16, 'English Daily turpis a', 'Russia', '1951-01-25'),
(235, 8, 'Lao Yearly in', 'Brazil', '1982-03-06'),
(236, 29, 'Persian Monthly lacinia', 'China', '1998-06-15'),
(237, 10, 'Filipino Never sem mauris', 'Greece', '1990-08-14'),
(238, 3, 'Dzongkha Never eget sempe', 'China', '1992-10-22'),
(239, 14, 'Latvian Weekly pulvinar l', 'China', '2008-09-29'),
(240, 18, 'French Never diam', 'Russia', '1979-06-05'),
(241, 28, 'Kyrgyz Monthly duis', 'China', '1952-05-15'),
(242, 6, 'Irish Gaelic Daily vivamu', 'Indonesia', '1939-05-13'),
(243, 5, 'Quechua Once nulla', 'Thailand', '2004-10-25'),
(244, 24, 'Aymara Once et', 'China', '1965-07-26'),
(245, 14, 'Gagauz Yearly faucibus', 'Japan', '2004-10-28'),
(246, 14, 'Latvian Once vulputate ju', 'Argentina', '1933-09-14'),
(247, 23, 'Malay Yearly congue risus', 'China', '1988-04-15'),
(248, 2, 'Tajik Daily felis ut', 'Brazil', '1950-04-14'),
(249, 29, 'Kazakh Monthly in hac', 'Democratic Repu', '1944-01-10'),
(250, 21, 'Greek Daily vestibulum', 'China', '1935-07-02'),
(251, 30, 'Korean Never id lobortis', 'Thailand', '1943-09-08'),
(252, 1, 'Romanian Once sit', 'Indonesia', '1968-04-20'),
(253, 8, 'Tajik Seldom etiam', 'China', '1988-12-15'),
(254, 13, 'Quechua Never augue', 'Brazil', '1948-06-24'),
(255, 5, 'Khmer Yearly amet', 'China', '1951-01-03'),
(256, 25, 'Gagauz Once porta volutpa', 'Kyrgyzstan', '2011-01-30'),
(257, 7, 'West Frisian Once eget or', 'China', '1951-01-30'),
(258, 14, 'Tok Pisin Once amet', 'Philippines', '2014-10-17'),
(259, 14, 'Spanish Never metus', 'Peru', '2000-12-26'),
(260, 27, 'Gujarati Never amet eros', 'Poland', '1989-07-21'),
(261, 2, 'Swati Seldom rutrum', 'Haiti', '1985-04-24'),
(262, 14, 'Italian Daily risus praes', 'Indonesia', '1971-07-08'),
(263, 8, 'Spanish Once semper rutru', 'China', '1951-05-20'),
(264, 3, 'Dari Yearly magna at', 'Russia', '1969-02-16'),
(265, 26, 'Spanish Monthly luctus', 'Indonesia', '1948-10-22'),
(266, 22, 'Ndebele Once sit amet', 'Macedonia', '1950-11-06'),
(267, 27, 'Persian Seldom etiam', 'Philippines', '1987-05-15'),
(268, 22, 'Hiri Motu Monthly vitae', 'China', '1938-11-10'),
(269, 13, 'Quechua Monthly luctus et', 'South Africa', '1951-07-16'),
(270, 22, 'Kashmiri Once lectus', 'Colombia', '1990-08-26'),
(271, 23, 'Dutch Monthly vulputate', 'Belarus', '1981-12-31'),
(272, 8, 'Papiamento Once dis partu', 'China', '2002-12-20'),
(273, 20, 'Hebrew Never rhoncus', 'China', '1930-04-21'),
(274, 23, 'Tamil Monthly magnis dis', 'Kazakhstan', '2006-10-21'),
(275, 25, 'Lao Monthly nisi volutpat', 'Ghana', '1964-01-09'),
(276, 18, 'Nepali Once luctus', 'American Samoa', '2012-01-07'),
(277, 2, 'Montenegrin Once ultrices', 'Mali', '2011-01-09'),
(278, 28, 'Tsonga Never ut', 'Russia', '1998-05-25'),
(279, 14, 'Kazakh Yearly faucibus or', 'Indonesia', '1975-09-22'),
(280, 20, 'Sotho Yearly dapibus dolo', 'Czech Republic', '2004-07-08'),
(281, 21, 'Moldovan Never cum sociis', 'Netherlands', '1930-03-17'),
(282, 17, 'Kurdish Once consequat', 'China', '1954-02-15'),
(283, 21, 'Dzongkha Yearly ut', 'Honduras', '1933-04-17'),
(284, 22, 'Italian Seldom sollicitud', 'Indonesia', '1988-04-18'),
(285, 13, 'Hiri Motu Yearly imperdie', 'China', '1935-11-20'),
(286, 27, 'Chinese Often ut', 'China', '1976-12-24'),
(287, 5, 'Aymara Never imperdiet', 'Ukraine', '1959-12-30'),
(288, 30, 'Croatian Yearly venenatis', 'Indonesia', '1972-08-07'),
(289, 18, 'Icelandic Once aliquam si', 'Indonesia', '1969-05-20'),
(290, 5, 'Greek Daily leo', 'Gambia', '1949-05-22'),
(291, 25, 'Sotho Daily augue', 'Indonesia', '1974-05-10'),
(292, 20, 'Tok Pisin Never mus vivam', 'Honduras', '1967-11-10'),
(293, 25, 'Dutch Never id massa', 'Afghanistan', '1970-12-30'),
(294, 17, 'Norwegian Once a nibh', 'Ethiopia', '1934-06-21'),
(295, 22, 'Afrikaans Often dui', 'Costa Rica', '1935-07-03'),
(296, 21, 'Czech Never elit sodales', 'Belarus', '1958-11-26'),
(297, 7, 'Croatian Weekly feugiat n', 'Serbia', '1979-05-14'),
(298, 28, 'Italian Weekly vivamus', 'Ireland', '1983-03-16'),
(299, 9, 'Lithuanian Never lorem', 'Canada', '1994-07-05'),
(300, 14, 'Papiamento Never a nibh', 'China', '2011-09-08'),
(301, 31, 'New one', 'Chicago', '2016-05-16'),
(302, 31, 'New two', 'Chicago', '2016-05-16');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `actors`
--
ALTER TABLE `actors`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`film_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `actors`
--
ALTER TABLE `actors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `films`
--
ALTER TABLE `films`
  MODIFY `film_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=303;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
