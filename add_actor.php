<?php 
include_once('db.php');

connect('localhost', 'root', '', 'actorsdb', function() {
	if ($_POST) {
		$name = $_POST['name'];
		$country = $_POST['country'];
		$birthdate = $_POST['birthdate'];
		$sql = "INSERT INTO actorsdb.actors (name, country, birthdate) VALUES('$name', '$country', '$birthdate')";

		request($sql, false, function() { header('Location: index.php'); });
	}
});	// Fconnect
?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Add actor</title>
	<link rel="stylesheet" type="text/css" href="static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme-flatly.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Add new actor</h3>
			</div>
			<div class="panel-body">
				<form action="<?php $_PHP_SELF ?>" method="POST" role="form">
					<div class="form-group">
						<label for="name">Name</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="John" />
					</div>
					<div class="form-group">
						<label for="country">Gender</label>
						<input type="text" class="form-control" id="country" name="country" size=6  placeholder="Chicago" />
					</div>
					<div class="form-group">
						<label for="birthdate">Birthdate</label>
						<input type="date" class="form-control" id="birthdate" name="birthdate" />
					</div>
					<div class="flex">
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-warning">Reset</button>
						<a href="index.php?id=2" class="btn btn-danger">Cancel</a>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>