<!-- Розробити сторінку для відображення реквізитів об’єкту та списку його елементів з реквізитами. Забезпечити зберігання/читання всіх необхідних вхідних даних в БД MySQL. Передбачити можливість редагування реквізитів об’єкту та додавання/зміни/видалення елементів списку. Змінити зовнішній вигляд елементів сторінки за допомогою використання динамічних таблиць стилів (CSS). Передбачити декілька варіантів відображення рядку таблиці (списку елементів) в залежності від значення якогось із реквізитів. -->
<!-- 11.	Актор (ПІБ, рік народження, країна)	Фільми (Назва, країна, рік виходу) -->

<?php
include_once('db.php');
$actors = [];
$id = 0;

if ($_POST) {
	$id = $_POST['actor'] - 1;
};

connect('localhost', 'root', '', 'actorsdb', function() {
	request('SELECT * FROM `actors`', true, function($response) {
		foreach ($response as $item) {
			$GLOBALS['actors'][] = $item;
		}
	});
});
?>
<html>
<head>
	<title>Indzilla.dev</title>
	<link rel="stylesheet" type="text/css" href="static/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme-flatly.min.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<div class="container">
	<!-- Choosing actor -->
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="controls-panel input-group">
				<h1 class="panel-title">
					<div class="controls-panel btn-group" role="group">
						<form action="<?php $_PHP_SELF ?>" method="POST">
							<select name="actor" id="actor" class="actors-list btn btn-primary" onchange="this.form.submit()">
								<option selected disabled>Choose Actor</option>
								<?php
								foreach ($actors as $actor) {
									if ($actor['id'] == ($id + 1))
										echo '<option selected value="'.$actor['id'].'">'.$actor['name'].'</option>';							
									else
										echo '<option value="'.$actor['id'].'">'.$actor['name'].'</option>';
								}
								?>
							</select>
						</form>
						<form action="add_actor.php" method="POST">
							<button type="submit" class="btn-margin btn btn-primary btn-sm">Add</button>
						</form>
						<form action="edit_actor.php?id=<?php echo $id + 1; ?>" method="POST">
							<button type="submit" class="btn-margin btn btn-primary btn-sm">Edit</button>
						</form>
						<form action="delete_actor.php?id=<?php echo $id + 1; ?>" method="POST" onsubmit="return confirm('Are You sure?');">
							<button type="submit" class="btn-margin btn btn-danger btn-sm">Delete</button>
						</form>
					</div>
				</h1>
			</div>
		</div>
		<div class="panel-body">
			<div class="flex actor-info">
				<img src="<?php echo $actors[$id]['photo']; ?>" width=150 alt="Here's <?php echo $actors[$id]['name']; ?>'s portrait" title="<?php echo $actors[$id]['name']; ?>" class="actor-photo">
				<div>
					<h2 class="text-danger"><?php echo $actors[$id]['name']; ?></h2>
					<h4>Birthdate: <span class="text-danger"><?php echo $actors[$id]['birthdate']; ?></span></h4>
					<h4>Country: <span class="text-danger"><?php echo $actors[$id]['country']; ?></span></h4>
					<a href="#actor-films" data-toggle='collapse' class="text-info">Show actor films</a>
				</div>
			</div>
			<ul id="actor-films" class="collapse actor-films">
				<?php
				connect('localhost', 'root', '', 'actorsdb', function() {
					$id = $GLOBALS['id'] + 1;
					$sql = "SELECT films.*, actors.id FROM actorsdb.actors INNER JOIN actorsdb.films ON actors.id = films.actor WHERE actors.id = $id";
					request($sql, true, function($response) {
						$id = 0;
						foreach ($response as $film) {
							$id++;
							$title = $film['title'];
							$country = $film['country'];
							$announced = $film['announced'];
							$film_id = $film['film_id'];

							echo "
							<li>
								<form action='edit_film.php?id=$film_id' method='POST' class='film-title'>
									<a href='#$id' data-toggle='collapse' data-parent='#actor-films'>$title</a>
									<button type='submit' class='edit-film glyphicon glyphicon-pencil'></button>
								</form>
								<p class='text-muted collapse' id='$id'>Announsed $announced in $country</p>
							</li>";
						}
					});
				});
				?>
				<li>
					<form action="add_film.php?actor_id=<?php echo $id + 1; ?>" method="POST">
						<button type="submit" class="text-primary text-btn">Add new one</button>
					</form>
				</li>
			</ul> <!-- actor-films -->
		</div> <!-- panel-body -->
	</div> <!-- panel-primary -->
</div>
</div>
</div>
<script type="text/javascript" src="static/jquery/jquery.min.js"></script>
<script type="text/javascript" src="static/bootstrap/js/bootstrap.min.js"></script>
<body>
</body>
</html>